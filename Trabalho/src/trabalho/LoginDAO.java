/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Carolini
 */
public class LoginDAO {
    
    Connection conexao;
    
    public LoginDAO(){
        conexao = ConexaoUtil.getConnection();
    }
    
   public  boolean validar(Usuario usuario) {
        String sql = "select * from usuario where login=? and senha=?";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            preparadorSQL.setString(1, usuario.getNlogin());
            preparadorSQL.setInt(2, usuario.getSenha());
            //Armazenando Resultado da consulta
            ResultSet resultado = preparadorSQL.executeQuery();
            if (resultado.next()) {
                
                preparadorSQL.close();
                return true;
            } else {
                return false;
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
}
