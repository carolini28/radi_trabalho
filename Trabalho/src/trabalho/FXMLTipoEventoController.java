/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package trabalho;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author ra143759
 */
public class FXMLTipoEventoController implements Initializable, ControlledScreen {

    ScreensController myController;

    //Gerenciador de dados // CRUD
    TipoEventoService eventoService = new TipoEventoService();
    
    @FXML
    private Label lbmsg;
    
    @FXML
    private TableView<TipoEvento> tvEvento;
    
    @FXML
    private TextField Tid;

    @FXML
    private TextField Tdescricao;
    
    @FXML
    private TableColumn IdEvento;
            
    @FXML
    private TableColumn descricaoEv;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    @FXML
     public void goToScreenP(ActionEvent event){//Menu
        myController.setScreen(Trabalho.screen1ID);
    }
     
    public void setScreenParent(ScreensController screenParent){
        myController = screenParent;
    }
    
    @FXML
    private void aoClicarBtnSalvar(ActionEvent event) {
       
        TipoEvento evento = new TipoEvento();
       
        if(!Tid.getText().isEmpty()){
            evento.setIdtipoevento(Integer.parseInt(Tid.getText()));
        }else{
            evento.setIdtipoevento(null);
        }
       
        evento.setDescricao(Tdescricao.getText());
        
        try {
            eventoService.salvar(evento);
            //Mensagem
            lbmsg.setText("Evento Salvo com Sucesso!");
        } catch (ServiceException ex) {
            lbmsg.setText(ex.getMessage());
            Logger.getLogger(FXMLTipoEventoController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
     @FXML
     private void aoClicarBtnExcluir(ActionEvent event) {
        Integer id = Integer.parseInt(Tid.getText());
        eventoService.excluir(id);
        lbmsg.setText("Evento Excluído com Sucesso!");
     }
     
     @FXML
     public void aoClicarBtnBuscarTodos(){

            tvEvento.setEditable(true);
            
            IdEvento.setCellValueFactory(new PropertyValueFactory<TipoEvento, String>("idtipoevento"));
                  
            descricaoEv.setCellValueFactory(new PropertyValueFactory<TipoEvento, String>("descricao"));
            
//            TableColumn id = new TableColumn("id");
//            id.setMinWidth(100);
//            id.setCellValueFactory(
//                    new PropertyValueFactory<TipoEvento, String>("idtipoevento"));
//
//
//            TableColumn descricao = new TableColumn("descricao");
//            descricao.setMinWidth(100);
//            descricao.setCellValueFactory(
//                    new PropertyValueFactory<TipoEvento, String>("descricao"));
//
//            tvEvento.getColumns().addAll(id, descricao);

            //Lista de Eventos
            List<TipoEvento> listaEvento =  eventoService.buscarTodos();
            //Inserindo a lista de um Observable
            final ObservableList<TipoEvento> dados = FXCollections.observableArrayList(listaEvento);

            tvEvento.setItems(dados);
            
            tvEvento.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observableValue, Object oldValue, Object newValue) {
                //Check whether item is selected and set value of selected item to Label
                if(tvEvento.getSelectionModel().getSelectedItem() != null) 
                {    
                   TableView.TableViewSelectionModel selectionModel = tvEvento.getSelectionModel();
                   ObservableList selectedCells = selectionModel.getSelectedCells();
                   TablePosition tablePosition = (TablePosition) selectedCells.get(0);
                   //Object val = tablePosition.getTableColumn().getCellData(newValue);
                   int row = tablePosition.getRow();
                   tvEvento.getSelectionModel().select(row);
                   TipoEvento evento = tvEvento.getSelectionModel().getSelectedItem();
                   Tid.setText(String.valueOf(evento.getIdtipoevento()));
                   Tdescricao.setText(evento.getDescricao());
                 }
              }
           });
        }
}
