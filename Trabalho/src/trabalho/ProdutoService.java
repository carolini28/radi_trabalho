/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package trabalho;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ra143759
 */
public class ProdutoService {
    
    private ProdutoDAO produtoDAO;

    public ProdutoService() {
        produtoDAO = new ProdutoDAO();
    }
    
    //Decide se vai guardar nova info ou alterar no ProdutoDAO
    public void salvar(Produto produto) throws ServiceException {
       
        if (produto.getDescricao().isEmpty()) {
            throw new ServiceException("Campo Descrição é Obrigatório!");
        }

        if (produto.getValor().isEmpty()) {
            throw new ServiceException("Campo Valor é Obrigatório!");
        }
     
        produtoDAO.salvar(produto);
    }
    
    public void excluir(Integer id) {
        produtoDAO.excluir(id);
    }
    
    public List<Produto> buscarTodos() {
            return produtoDAO.buscarTodos();
    }
  
    public ArrayList<String> buscarProdutos(){
        return produtoDAO.BuscarProdutos();
    }
}
