/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho;

import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author Carolini
 */
public class FXMLAgendaController implements Initializable, ControlledScreen {
    
    ScreensController myController;
    ClienteService cliService = new ClienteService();
    PedidoService pedidoService = new PedidoService();
    AgendaService agendaService = new AgendaService();
    
     @FXML
     private ComboBox Pcliente;
     
     @FXML
     private TextField dataI_evento;
     
     @FXML
     private TextField dataF_evento;
     
     @FXML
     private TextField dataI_contrato;
     
     @FXML
     private TextField dataF_contrato;
     
     @FXML
     private Label lbmsg;
     
     @FXML
     private TableView<Agenda> tvPedidos;
     
     @FXML 
     private TableColumn idPedido;
     
     @FXML
     private TableColumn clientePedido;
     
     @FXML
     private TableColumn dataCPedido;
     
     @FXML
     private TableColumn valorPedido;
     
     @FXML
     private TableColumn eventoPedido;
     
     @FXML
     private TableColumn eventoDPedido;
     
     @FXML
     private TableColumn produtoPedido;
     
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        Pcliente.setEditable(true);
        
        ArrayList<String>  list = cliService.buscarNomes();
         
        ObservableList<String> options =  FXCollections.observableArrayList(list);
  
        Pcliente.setItems(options);
    }
    
    @FXML
    public void aoClicarBtnBuscar (ActionEvent event) throws ParseException{
           
        if(Pcliente.getValue().toString().isEmpty()){
            
            if(!dataI_evento.getText().isEmpty() && !dataF_evento.getText().isEmpty()){
                       
                  if(!dataI_contrato.getText().isEmpty() && !dataF_contrato.getText().isEmpty()){
                      //fazer buca por data contrato e data evento
                      String n1 = new String(dataI_evento.getText());
                      String n2 = new String(dataF_evento.getText());
                      
                      SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");  
                      java.sql.Date data1 = new java.sql.Date(format.parse(n1).getTime());
                      java.sql.Date data2 = new java.sql.Date(format.parse(n2).getTime());
                      
//                      String s1[] = n1.split("/");   
//                      String data1 = s1[2]+"-"+s1[1]+"-"+s1[0];
//
//                      String s2[] = n2.split("/");   
//                      String data2 = s2[2]+"-"+s2[1]+"-"+s2[0];
                      
                      String n3 = new String(dataI_contrato.getText());
                      String n4 = new String(dataF_contrato.getText());
                       
                      java.sql.Date data3 = new java.sql.Date(format.parse(n3).getTime());
                      java.sql.Date data4 = new java.sql.Date(format.parse(n4).getTime());

//                      String s3[] = n3.split("/");   
//                      String data3 = s3[2]+"-"+s3[1]+"-"+s3[0];
//
//                      String s4[] = n4.split("/");   
//                      String data4 = s4[2]+"-"+s4[1]+"-"+s4[0];
                      
                      
                      tvPedidos.setEditable(true);   
                      
                      idPedido.setCellValueFactory(new PropertyValueFactory<Agenda, Integer>("idpedido"));
     
                      clientePedido.setCellValueFactory(new PropertyValueFactory<Agenda, String>("nomeCliente"));
     
                      dataCPedido.setCellValueFactory(new PropertyValueFactory<Agenda, String>("dataContrato"));
     
                      valorPedido.setCellValueFactory(new PropertyValueFactory<Agenda, Double>("valor"));
     
                      eventoPedido.setCellValueFactory(new PropertyValueFactory<Agenda, String>("tipoEvento"));
     
                      eventoDPedido.setCellValueFactory(new PropertyValueFactory<Agenda, String>("dataEvento"));
     
                      produtoPedido.setCellValueFactory(new PropertyValueFactory<Agenda, Integer>("quantidadeP"));
                      
                      List<Agenda> listaAgenda = agendaService.buscarAgenda(null, data1, data2, data3, data4, 6);
                      //Inserindo a lista de um Observable
                      final ObservableList<Agenda> dados = FXCollections.observableArrayList(listaAgenda);
                      tvPedidos.setItems(dados);
                      
                  }else if(dataI_contrato.getText() != null && dataF_contrato.getText() == null){
                      lbmsg.setText("Inserir data final do contrato!");
                  }else if(dataI_contrato.getText() == null && dataF_contrato.getText() != null){
                      lbmsg.setText("Inserir data inicial do contrato!");
                  }else{
                      String n1 = new String(dataI_evento.getText());
                      String n2 = new String(dataF_evento.getText());
                      
                      SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");  
                      java.sql.Date data1 = new java.sql.Date(format.parse(n1).getTime());
                      java.sql.Date data2 = new java.sql.Date(format.parse(n2).getTime());
                      //System.out.println("evento: "+ data1 +""+data2);
            
//                      String s1[] = n1.split("/");   
//                      String data1 = s1[2]+"-"+s1[1]+"-"+s1[0];
//
//                      String s2[] = n2.split("/");   
//                      String data2 = s2[2]+"-"+s2[1]+"-"+s2[0];
                      
                      //fazer buca por data evento                     
                      tvPedidos.setEditable(true);            
                      
                      idPedido.setCellValueFactory(new PropertyValueFactory<Agenda, Integer>("idpedido"));
     
                      clientePedido.setCellValueFactory(new PropertyValueFactory<Agenda, String>("nomeCliente"));
     
                      dataCPedido.setCellValueFactory(new PropertyValueFactory<Agenda, String>("dataContrato"));
     
                      valorPedido.setCellValueFactory(new PropertyValueFactory<Agenda, Double>("valor"));
     
                      eventoPedido.setCellValueFactory(new PropertyValueFactory<Agenda, String>("tipoEvento"));
     
                      eventoDPedido.setCellValueFactory(new PropertyValueFactory<Agenda, String>("dataEvento"));
     
                      produtoPedido.setCellValueFactory(new PropertyValueFactory<Agenda, Integer>("quantidadeP"));
                      
                      List<Agenda> listaAgenda = agendaService.buscarAgenda(null, data1, data2, null, null, 3);
                      //Inserindo a lista de um Observable
                      final ObservableList<Agenda> dados = FXCollections.observableArrayList(listaAgenda);
                      tvPedidos.setItems(dados);
                  }
                
            }else if(dataI_evento.getText()!=null && dataF_evento.getText()==null){
                lbmsg.setText("Inserir data final do evento!");
            }else if(dataI_evento.getText()==null && dataF_evento.getText()!=null){
                lbmsg.setText("Inserir data inicial do evento!");
            }else{
                            
                  if(dataI_contrato.getText() != null && dataF_contrato.getText() != null){
                       //fazer busca por data do contrato 
                      
                      String n1 = new String(dataI_contrato.getText());
                      String n2 = new String(dataF_contrato.getText());
                      
                      SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");  
                      java.sql.Date data1 = new java.sql.Date(format.parse(n1).getTime());
                      java.sql.Date data2 = new java.sql.Date(format.parse(n2).getTime());
//                      
//                      String s1[] = n1.split("/");   
//                      String data1 = s1[2]+"-"+s1[1]+"-"+s1[0];
//
//                      String s2[] = n2.split("/");   
//                      String data2 = s2[2]+"-"+s2[1]+"-"+s2[0];
                      
                      tvPedidos.setEditable(true);              
                      
                      idPedido.setCellValueFactory(new PropertyValueFactory<Agenda, Integer>("idpedido"));
     
                      clientePedido.setCellValueFactory(new PropertyValueFactory<Agenda, String>("nomeCliente"));
     
                      dataCPedido.setCellValueFactory(new PropertyValueFactory<Agenda, String>("dataContrato"));
     
                      valorPedido.setCellValueFactory(new PropertyValueFactory<Agenda, Double>("valor"));
     
                      eventoPedido.setCellValueFactory(new PropertyValueFactory<Agenda, String>("tipoEvento"));
     
                      eventoDPedido.setCellValueFactory(new PropertyValueFactory<Agenda, String>("dataEvento"));
     
                      produtoPedido.setCellValueFactory(new PropertyValueFactory<Agenda, Integer>("quantidadeP"));
                      
                      List<Agenda> listaAgenda = agendaService.buscarAgenda(null, null, null, data1, data2, 2);
                      //Inserindo a lista de um Observable
                      final ObservableList<Agenda> dados = FXCollections.observableArrayList(listaAgenda);
                      tvPedidos.setItems(dados);
                      
                  }else if(dataI_contrato.getText() != null && dataF_contrato.getText() == null){
                      lbmsg.setText("Inserir data final do contrato!");
                  }else if(dataI_contrato.getText() == null && dataF_contrato.getText() != null){
                      lbmsg.setText("Inserir data inicial do contrato!");
                  }else{
                      lbmsg.setText("Todos os campos Vazios!");
                  }
            }
           
        }else{
            //verificar data evento
            System.out.println("cliente: "+ Pcliente.getValue().toString());
            if(!dataI_evento.getText().isEmpty() && !dataF_evento.getText().isEmpty()){
                
                  if(!dataI_contrato.getText().isEmpty() && !dataF_contrato.getText().isEmpty()){
                      //fazer busca por cliente e evento e contrato
                      String n1 = new String(dataI_evento.getText());
                      String n2 = new String(dataF_evento.getText());
                      
                      SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");  
                      java.sql.Date data1 = new java.sql.Date(format.parse(n1).getTime());
                      java.sql.Date data2 = new java.sql.Date(format.parse(n2).getTime());
                      
//                      String s1[] = n1.split("/");   
//                      String data1 = s1[2]+"-"+s1[1]+"-"+s1[0];
//
//                      String s2[] = n2.split("/");   
//                      String data2 = s2[2]+"-"+s2[1]+"-"+s2[0];
                      
                      String n3 = new String(dataI_contrato.getText());
                      String n4 = new String(dataF_contrato.getText());
                              
                      
                      java.sql.Date data3 = new java.sql.Date(format.parse(n3).getTime());
                      java.sql.Date data4 = new java.sql.Date(format.parse(n4).getTime());        

//                      String s3[] = n3.split("/");   
//                      String data3 = s3[2]+"-"+s3[1]+"-"+s3[0];
//
//                      String s4[] = n4.split("/");   
//                      String data4 = s4[2]+"-"+s4[1]+"-"+s4[0];
                      
                      tvPedidos.setEditable(true);                
                      
                      idPedido.setCellValueFactory(new PropertyValueFactory<Agenda, Integer>("idpedido"));
     
                      clientePedido.setCellValueFactory(new PropertyValueFactory<Agenda, String>("nomeCliente"));
     
                      dataCPedido.setCellValueFactory(new PropertyValueFactory<Agenda, String>("dataContrato"));
     
                      valorPedido.setCellValueFactory(new PropertyValueFactory<Agenda, Double>("valor"));
     
                      eventoPedido.setCellValueFactory(new PropertyValueFactory<Agenda, String>("tipoEvento"));
     
                      eventoDPedido.setCellValueFactory(new PropertyValueFactory<Agenda, String>("dataEvento"));
     
                      produtoPedido.setCellValueFactory(new PropertyValueFactory<Agenda, Integer>("quantidadeP"));
                      
                      List<Agenda> listaAgenda = agendaService.buscarAgenda(Pcliente.getValue().toString(), data1, data2, data3, data4, 7);
                      //Inserindo a lista de um Observable
                      final ObservableList<Agenda> dados = FXCollections.observableArrayList(listaAgenda);
                      tvPedidos.setItems(dados);
                      
                  }else if(!dataI_contrato.getText().isEmpty() && dataF_contrato.getText().isEmpty()){
                      lbmsg.setText("Inserir data final do contrato!");
                  }else if(dataI_contrato.getText().isEmpty() && !dataF_contrato.getText().isEmpty()){
                      lbmsg.setText("Inserir data inicial do contrato!");
                  }else{
                       //fazer busca por cliente e evento
                      String n1 = new String(dataI_evento.getText());
                      String n2 = new String(dataF_evento.getText());
                      
                      SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");  
                      java.sql.Date data1 = new java.sql.Date(format.parse(n1).getTime());
                      java.sql.Date data2 = new java.sql.Date(format.parse(n2).getTime());
//                      
//                      String s1[] = n1.split("/");   
//                      String data1 = s1[2]+"-"+s1[1]+"-"+s1[0];
//
//                      String s2[] = n2.split("/");   
//                      String data2 = s2[2]+"-"+s2[1]+"-"+s2[0];
                      
                      tvPedidos.setEditable(true);                   
                      
                      idPedido.setCellValueFactory(new PropertyValueFactory<Agenda, Integer>("idpedido"));
     
                      clientePedido.setCellValueFactory(new PropertyValueFactory<Agenda, String>("nomeCliente"));
     
                      dataCPedido.setCellValueFactory(new PropertyValueFactory<Agenda, String>("dataContrato"));
     
                      valorPedido.setCellValueFactory(new PropertyValueFactory<Agenda, Double>("valor"));
     
                      eventoPedido.setCellValueFactory(new PropertyValueFactory<Agenda, String>("tipoEvento"));
     
                      eventoDPedido.setCellValueFactory(new PropertyValueFactory<Agenda, String>("dataEvento"));
     
                      produtoPedido.setCellValueFactory(new PropertyValueFactory<Agenda, Integer>("quantidadeP"));
                      
                      List<Agenda> listaAgenda = agendaService.buscarAgenda(Pcliente.getValue().toString(), data1, data2, null, null, 5);
                      //Inserindo a lista de um Observable
                      final ObservableList<Agenda> dados = FXCollections.observableArrayList(listaAgenda);
                      tvPedidos.setItems(dados);
                  }   
                  
            }else if(!dataI_evento.getText().isEmpty() && dataF_evento.getText().isEmpty()){
                lbmsg.setText("Inserir data final do evento!");
            }else if(dataI_evento.getText().isEmpty() && !dataF_evento.getText().isEmpty()){
                lbmsg.setText("Inserir data inicial do evento!");
            }else{
                
                  if(!dataI_contrato.getText().isEmpty() && !dataF_contrato.getText().isEmpty()){
                       //fazer busca por data do contrato e cliente
                      
                      String n3 = new String(dataI_contrato.getText());
                      String n4 = new String(dataF_contrato.getText());

                      SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");  
                      java.sql.Date data3 = new java.sql.Date(format.parse(n3).getTime());
                      java.sql.Date data4 = new java.sql.Date(format.parse(n4).getTime());
//                      String s3[] = n3.split("/");   
//                      String data3 = s3[2]+"-"+s3[1]+"-"+s3[0];
//
//                      String s4[] = n4.split("/");   
//                      String data4 = s4[2]+"-"+s4[1]+"-"+s4[0];
                      
                      tvPedidos.setEditable(true);     
                      
                      idPedido.setCellValueFactory(new PropertyValueFactory<Agenda, Integer>("idpedido"));
     
                      clientePedido.setCellValueFactory(new PropertyValueFactory<Agenda, String>("nomeCliente"));
     
                      dataCPedido.setCellValueFactory(new PropertyValueFactory<Agenda, String>("dataContrato"));
     
                      valorPedido.setCellValueFactory(new PropertyValueFactory<Agenda, Double>("valor"));
     
                      eventoPedido.setCellValueFactory(new PropertyValueFactory<Agenda, String>("tipoEvento"));
     
                      eventoDPedido.setCellValueFactory(new PropertyValueFactory<Agenda, String>("dataEvento"));
     
                      produtoPedido.setCellValueFactory(new PropertyValueFactory<Agenda, Integer>("quantidadeP"));
                      
                      
                      List<Agenda> listaAgenda = agendaService.buscarAgenda(Pcliente.getValue().toString(), null, null, data3, data4, 4);
                      //Inserindo a lista de um Observable
                      final ObservableList<Agenda> dados = FXCollections.observableArrayList(listaAgenda);
                      tvPedidos.setItems(dados);
                      
                  }else if(!dataI_contrato.getText().isEmpty() && dataF_contrato.getText().isEmpty()){
                      lbmsg.setText("Inserir data final do contrato!");
                  }else if(dataI_contrato.getText().isEmpty() && !dataF_contrato.getText().isEmpty()){
                      lbmsg.setText("Inserir data inicial do contrato!");
                  }else{
                      //fazer busca por cliente
                      tvPedidos.setEditable(true); 
                      
                      idPedido.setCellValueFactory(new PropertyValueFactory<Agenda, Integer>("idpedido"));
     
                      clientePedido.setCellValueFactory(new PropertyValueFactory<Agenda, String>("nomeCliente"));
     
                      dataCPedido.setCellValueFactory(new PropertyValueFactory<Agenda, String>("dataContrato"));
     
                      valorPedido.setCellValueFactory(new PropertyValueFactory<Agenda, Double>("valor"));
     
                      eventoPedido.setCellValueFactory(new PropertyValueFactory<Agenda, String>("tipoEvento"));
     
                      eventoDPedido.setCellValueFactory(new PropertyValueFactory<Agenda, String>("dataEvento"));
     
                      produtoPedido.setCellValueFactory(new PropertyValueFactory<Agenda, Integer>("quantidadeP"));
                      
                      List<Agenda> listaAgenda = agendaService.buscarAgenda(Pcliente.getValue().toString(), null, null, null, null, 1);
                      //Inserindo a lista de um Observable
                      final ObservableList<Agenda> dados = FXCollections.observableArrayList(listaAgenda);
                      tvPedidos.setItems(dados);
                      
                  }
            }
        }
    
    }
    
    
    
    
    
    @FXML
     public void goToScreenP(ActionEvent event){
        myController.setScreen(Trabalho.screen8ID);
    }

    @Override
    public void setScreenParent(ScreensController screenParent) {
         myController = screenParent;
    }
}
