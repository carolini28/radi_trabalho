/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package trabalho;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ra143759
 */
public class TipoEventoDAO {
 
    Connection conexao;
    
    public TipoEventoDAO(){
        conexao = ConexaoUtil.getConnection();
    }
    
    public void salvar(TipoEvento evento) {
        if (evento.getIdtipoevento() == null) {
            cadastrar(evento);
        } else {
            alterar(evento);
        }
   }

    private void cadastrar(TipoEvento evento) {
      String sql = "insert  into tipoevento (descricao) values (?)";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            preparadorSQL.setString(1, evento.getDescricao());
 
            preparadorSQL.execute();
            preparadorSQL.close();
        } catch (SQLException ex) {
            Logger.getLogger(TipoEventoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void alterar(TipoEvento evento) {
         String sql = "update tipoevento set descricao=? where idtipoevento=?";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            preparadorSQL.setString(1, evento.getDescricao());
            preparadorSQL.setInt(2, evento.getIdtipoevento());
      
            preparadorSQL.execute();
            preparadorSQL.close();
        } catch (SQLException ex) {
            Logger.getLogger(TipoEventoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void excluir(Integer id) {
        String sql = "delete from tipoevento where idtipoevento=?";

        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            preparadorSQL.setInt(1, id);

            preparadorSQL.execute();
            preparadorSQL.close();
        } catch (SQLException ex) {
            Logger.getLogger(TipoEventoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<TipoEvento> buscarTodos() {
        String sql = "select * from tipoevento order by idtipoevento";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            //Armazenando Resultado da consulta
            ResultSet resultado = preparadorSQL.executeQuery();
            List<TipoEvento> lista = new ArrayList<TipoEvento>();
            while (resultado.next()) {
                //Instancia de eventos
                TipoEvento evento = new TipoEvento();

                //Atribuindo dados do resultado no objeto evento
                evento.setIdtipoevento(resultado.getInt("idtipoevento"));
                evento.setDescricao(resultado.getString("descricao"));
                
                //Adicionando evento na lista
                lista.add(evento);
            }
            
            preparadorSQL.close();
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(TipoEventoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
}
