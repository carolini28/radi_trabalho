/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho;

import com.sun.deploy.util.StringUtils;
import java.util.List;

/**
 *
 * @author Carolini
 */
public class UsuarioService {
    
    private UsuarioDAO usuarioDAO;
    boolean ehNumero = true;
    
    public UsuarioService(){
      usuarioDAO = new UsuarioDAO();
    }
    
    //Decide se vai guardar nova info ou alterar no UsuarioDAO
    public void salvar(Usuario usuario) throws ServiceException {
       
        if (usuario.getNome().isEmpty()) {
            throw new ServiceException("Campo Nome é Obrigatório!");
        }

        if (usuario.getEmail().isEmpty()) {
            throw new ServiceException("Campo Email é Obrigatório!");
        }
        
        if(usuario.getNlogin().isEmpty()){
           throw new ServiceException("Campo Email é Obrigatório!");
        }
     
        if(usuario.getSenha() == null){
          throw new ServiceException("Campo senha é Obrigatório!");
        }
        
     
        usuarioDAO.salvar(usuario);
    }
    
    public void excluir(Integer id) {
        usuarioDAO.excluir(id);
    }
    
    public List<Usuario> buscarTodos() {
            return usuarioDAO.buscarTodos();
    }
}
