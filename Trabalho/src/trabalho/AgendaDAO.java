/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Carolini
 */
public class AgendaDAO {
    
    Connection conexao;
    
    public AgendaDAO(){
        conexao = ConexaoUtil.getConnection();
    }
    
    public List<Agenda> buscar(String cliente, Date dataIE, Date dataFE, Date dataIC, Date dataFC, int busca) throws ParseException {
        
        //cliente
        if(busca == 1){
            System.out.println("cliente: "+ cliente);
           String sql = "SELECT SUM(itempedido.quantidade) quantidade, pedido.dataevento, descricao,  pedido.datapedido, nome, itempedido.idpedido, SUM(itempedido.valor) total FROM itempedido, pedido, cliente, tipoevento WHERE itempedido.idpedido = pedido.idpedido and pedido.idcliente = cliente.idcliente and pedido.idtipoevento = tipoevento.idtipoevento and cliente.nome=? GROUP BY itempedido.idpedido, nome, pedido.datapedido, descricao, pedido.dataevento ORDER BY  pedido.datapedido"; 
                try {
                    PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
                    preparadorSQL.setString(1, cliente);
                    //Armazenando Resultado da consulta
                    ResultSet resultado = preparadorSQL.executeQuery();
                    List<Agenda> lista = new ArrayList<Agenda>();
                    while (resultado.next()) {
                        System.out.println("cliente: "+ cliente);
                        //Instancia de agenda
                        Agenda agenda = new Agenda();
                        //Atribuindo dados do resultado no objeto agenda
                        agenda.setQuantidadeP(resultado.getInt("quantidade"));
                        agenda.setDataEvento(resultado.getString("dataevento"));
                        agenda.setTipoEvento(resultado.getString("descricao"));
                        agenda.setDataContrato(resultado.getString("datapedido"));
                        agenda.setNomeCliente(resultado.getString("nome"));
                        agenda.setIdpedido(resultado.getInt("idpedido"));
                        agenda.setValor(resultado.getDouble("total"));
                       
                        lista.add(agenda);
                    } 
                    
                     preparadorSQL.close();
                     return lista;
                } catch (SQLException ex) {
                    Logger.getLogger(AgendaDAO.class.getName()).log(Level.SEVERE, null, ex);
                    return null;
                }

         // data contrato       
        }else if(busca == 2){
            String sql = "SELECT SUM(itempedido.quantidade) quantidade, pedido.dataevento, descricao,  pedido.datapedido, nome, itempedido.idpedido pedido, SUM(itempedido.valor) valor FROM itempedido, pedido, cliente, tipoevento WHERE itempedido.idpedido = pedido.idpedido and pedido.idcliente = cliente.idcliente and pedido.idtipoevento = tipoevento.idtipoevento and pedido.datapedido BETWEEN ? and ? GROUP BY itempedido.idpedido, nome, pedido.datapedido, descricao, pedido.dataevento ORDER BY  pedido.datapedido";
            try {
                    PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
                    preparadorSQL.setDate(1, (java.sql.Date) dataIC);
                    preparadorSQL.setDate(2, (java.sql.Date) dataFC);
                    //Armazenando Resultado da consulta
                    ResultSet resultado = preparadorSQL.executeQuery();
                    List<Agenda> lista = new ArrayList<Agenda>();
                    while (resultado.next()) {
                        //Instancia de agenda
                        Agenda agenda = new Agenda();
                        //Atribuindo dados do resultado no objeto agenda
                        agenda.setIdpedido(resultado.getInt("pedido"));
                        agenda.setNomeCliente(resultado.getString("nome"));
                        agenda.setDataContrato(resultado.getString("datapedido"));
                        agenda.setValor(resultado.getDouble("valor"));
                        agenda.setTipoEvento(resultado.getString("descricao"));
                        agenda.setDataEvento(resultado.getString("dataevento"));
                        agenda.setQuantidadeP(resultado.getInt("quantidade"));
                       
                        lista.add(agenda);
                    } 
                    
                     preparadorSQL.close();
                     return lista;
                } catch (SQLException ex) {
                    Logger.getLogger(AgendaDAO.class.getName()).log(Level.SEVERE, null, ex);
                    return null;
                }
         //data evento   
        }else if(busca == 3){
            
            System.out.println("evento: "+ dataIE +""+dataFE);
            
            String sql = "SELECT SUM(itempedido.quantidade) quantidade, pedido.dataevento, descricao,  pedido.datapedido, nome, itempedido.idpedido pedido, SUM(itempedido.valor) valor FROM itempedido, pedido, cliente, tipoevento WHERE itempedido.idpedido = pedido.idpedido and pedido.idcliente = cliente.idcliente and pedido.idtipoevento = tipoevento.idtipoevento and pedido.dataevento BETWEEN ? and ? GROUP BY itempedido.idpedido, nome, pedido.datapedido, descricao, pedido.dataevento ORDER BY  pedido.datapedido";
            try {
                    PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
                    preparadorSQL.setDate(1, (java.sql.Date) dataIE);
                    preparadorSQL.setDate(2, (java.sql.Date) dataFE);
                    //Armazenando Resultado da consulta
                    ResultSet resultado = preparadorSQL.executeQuery();
                    List<Agenda> lista = new ArrayList<Agenda>();
                    while (resultado.next()) {
                        //Instancia de agenda
                        Agenda agenda = new Agenda();
                        //Atribuindo dados do resultado no objeto agenda
                        agenda.setIdpedido(resultado.getInt("pedido"));
                        agenda.setNomeCliente(resultado.getString("nome"));
                        agenda.setDataContrato(resultado.getString("datapedido"));
                        agenda.setValor(resultado.getDouble("valor"));
                        agenda.setTipoEvento(resultado.getString("descricao"));
                        agenda.setDataEvento(resultado.getString("dataevento"));
                        agenda.setQuantidadeP(resultado.getInt("quantidade"));
                       
                        lista.add(agenda);
                    } 
                    
                     preparadorSQL.close();
                     return lista;
                } catch (SQLException ex) {
                    Logger.getLogger(AgendaDAO.class.getName()).log(Level.SEVERE, null, ex);
                    return null;
                }
         //cliente e data de contrato   
        }else if(busca == 4){
            String sql = "SELECT SUM(itempedido.quantidade) quantidade, pedido.dataevento, descricao,  pedido.datapedido, nome, itempedido.idpedido pedido, SUM(itempedido.valor) valor FROM itempedido, pedido, cliente, tipoevento WHERE itempedido.idpedido = pedido.idpedido and pedido.idcliente = cliente.idcliente and pedido.idtipoevento = tipoevento.idtipoevento and pedido.datapedido BETWEEN ? and ? and cliente.nome=? GROUP BY itempedido.idpedido, nome, pedido.datapedido, descricao, pedido.dataevento ORDER BY  pedido.datapedido";
            try {
                    PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
                    
                    preparadorSQL.setDate(1, (java.sql.Date) dataIC);
                    preparadorSQL.setDate(2, (java.sql.Date) dataFC);
                    preparadorSQL.setString(3, cliente);
                    //Armazenando Resultado da consulta
                    ResultSet resultado = preparadorSQL.executeQuery();
                    List<Agenda> lista = new ArrayList<Agenda>();
                    while (resultado.next()) {
                        //Instancia de agenda
                        Agenda agenda = new Agenda();
                        //Atribuindo dados do resultado no objeto agenda
                        agenda.setIdpedido(resultado.getInt("pedido"));
                        agenda.setNomeCliente(resultado.getString("nome"));
                        agenda.setDataContrato(resultado.getString("datapedido"));
                        agenda.setValor(resultado.getDouble("valor"));
                        agenda.setTipoEvento(resultado.getString("descricao"));
                        agenda.setDataEvento(resultado.getString("dataevento"));
                        agenda.setQuantidadeP(resultado.getInt("quantidade"));
                       
                        lista.add(agenda);
                    } 
                    
                     preparadorSQL.close();
                     return lista;
                } catch (SQLException ex) {
                    Logger.getLogger(AgendaDAO.class.getName()).log(Level.SEVERE, null, ex);
                    return null;
                }
          //cliente e data de evento  
        }else if(busca == 5){
        
            String sql = "SELECT SUM(itempedido.quantidade) quantidade, pedido.dataevento, descricao,  pedido.datapedido, nome, itempedido.idpedido pedido, SUM(itempedido.valor) valor FROM itempedido, pedido, cliente, tipoevento WHERE itempedido.idpedido = pedido.idpedido and pedido.idcliente = cliente.idcliente and pedido.idtipoevento = tipoevento.idtipoevento and pedido.dataevento BETWEEN ? and ? and cliente.nome=? GROUP BY itempedido.idpedido, nome, pedido.datapedido, descricao, pedido.dataevento ORDER BY  pedido.datapedido";
            try {
                    PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
                    
                    preparadorSQL.setDate(1, (java.sql.Date) dataIE);
                    preparadorSQL.setDate(2, (java.sql.Date) dataFE);
                    preparadorSQL.setString(3, cliente);
                    //Armazenando Resultado da consulta
                    ResultSet resultado = preparadorSQL.executeQuery();
                    List<Agenda> lista = new ArrayList<Agenda>();
                    while (resultado.next()) {
                        //Instancia de agenda
                        Agenda agenda = new Agenda();
                        //Atribuindo dados do resultado no objeto agenda
                        agenda.setIdpedido(resultado.getInt("pedido"));
                        agenda.setNomeCliente(resultado.getString("nome"));
                        agenda.setDataContrato(resultado.getString("datapedido"));
                        agenda.setValor(resultado.getDouble("valor"));
                        agenda.setTipoEvento(resultado.getString("descricao"));
                        agenda.setDataEvento(resultado.getString("dataevento"));
                        agenda.setQuantidadeP(resultado.getInt("quantidade"));
                       
                        lista.add(agenda);
                    } 
                     preparadorSQL.close();
                     return lista;
                } catch (SQLException ex) {
                    Logger.getLogger(AgendaDAO.class.getName()).log(Level.SEVERE, null, ex);
                    return null;
                }
            //contrato && evento
        }else if(busca == 6){
            
            String sql = "SELECT SUM(itempedido.quantidade) quantidade, pedido.dataevento, descricao,  pedido.datapedido, nome, itempedido.idpedido pedido, SUM(itempedido.valor) valor FROM itempedido, pedido, cliente, tipoevento WHERE itempedido.idpedido = pedido.idpedido and pedido.idcliente = cliente.idcliente and pedido.idtipoevento = tipoevento.idtipoevento and pedido.dataevento BETWEEN ? and ? and pedido.datapedido BETWEEN ? and ? GROUP BY itempedido.idpedido, nome, pedido.datapedido, descricao, pedido.dataevento ORDER BY  pedido.datapedido";
            try {
                    PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
                    preparadorSQL.setDate(1, (java.sql.Date) dataIE);
                    preparadorSQL.setDate(2, (java.sql.Date) dataFE);
                    preparadorSQL.setDate(3, (java.sql.Date) dataIC);
                    preparadorSQL.setDate(4, (java.sql.Date) dataFC);
                    //Armazenando Resultado da consulta
                    ResultSet resultado = preparadorSQL.executeQuery();
                    List<Agenda> lista = new ArrayList<Agenda>();
                    while (resultado.next()) {
                        //Instancia de agenda
                        Agenda agenda = new Agenda();
                        //Atribuindo dados do resultado no objeto agenda
                        agenda.setIdpedido(resultado.getInt("pedido"));
                        agenda.setNomeCliente(resultado.getString("nome"));
                        agenda.setDataContrato(resultado.getString("datapedido"));
                        agenda.setValor(resultado.getDouble("valor"));
                        agenda.setTipoEvento(resultado.getString("descricao"));
                        agenda.setDataEvento(resultado.getString("dataevento"));
                        agenda.setQuantidadeP(resultado.getInt("quantidade"));
                       
                        lista.add(agenda);
                    } 
                    
                     preparadorSQL.close();
                     return lista;
                } catch (SQLException ex) {
                    Logger.getLogger(AgendaDAO.class.getName()).log(Level.SEVERE, null, ex);
                    return null;
                }
            
        }else{
            String sql = "SELECT SUM(itempedido.quantidade) quantidade, pedido.dataevento, descricao,  pedido.datapedido, nome, itempedido.idpedido pedido, SUM(itempedido.valor) valor FROM itempedido, pedido, cliente, tipoevento WHERE itempedido.idpedido = pedido.idpedido and pedido.idcliente = cliente.idcliente and pedido.idtipoevento = tipoevento.idtipoevento and pedido.dataevento BETWEEN ? and ? and pedido.datapedido BETWEEN ? and ? and cliente.nome =? GROUP BY itempedido.idpedido, nome, pedido.datapedido, descricao, pedido.dataevento ORDER BY  pedido.datapedido";
            try {
                    PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
                    preparadorSQL.setDate(1, (java.sql.Date) dataIE);
                    preparadorSQL.setDate(2, (java.sql.Date) dataFE);
                    preparadorSQL.setDate(3, (java.sql.Date) dataIC);
                    preparadorSQL.setDate(4, (java.sql.Date) dataFC);
                    preparadorSQL.setString(5, cliente);
                    //Armazenando Resultado da consulta
                    ResultSet resultado = preparadorSQL.executeQuery();
                    List<Agenda> lista = new ArrayList<Agenda>();
                    while (resultado.next()) {
                        //Instancia de agenda
                        Agenda agenda = new Agenda();
                        //Atribuindo dados do resultado no objeto agenda
                        agenda.setIdpedido(resultado.getInt("pedido"));
                        agenda.setNomeCliente(resultado.getString("nome"));
                        agenda.setDataContrato(resultado.getString("datapedido"));
                        agenda.setValor(resultado.getDouble("valor"));
                        agenda.setTipoEvento(resultado.getString("descricao"));
                        agenda.setDataEvento(resultado.getString("dataevento"));
                        agenda.setQuantidadeP(resultado.getInt("quantidade"));
                       
                        lista.add(agenda);
                    } 
                    
                     preparadorSQL.close();
                     return lista;
                } catch (SQLException ex) {
                    Logger.getLogger(AgendaDAO.class.getName()).log(Level.SEVERE, null, ex);
                    return null;
                }
        }
       
       
    }
}
