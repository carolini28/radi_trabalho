/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Carolini
 */
public class FXMLloginController implements Initializable,ControlledScreen {

    ScreensController myController;
    
    @FXML
    private TextField Tlogin;
             
    @FXML
    private TextField Tsenha;
    
    @FXML
    private Label lbmsg;
    
    //Gerenciador de dados // CRUD
    LoginService loginService = new LoginService();
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    @FXML
     public void goToScreenP(ActionEvent event){
        myController.setScreen(Trabalho.screen8ID);
    }

    @Override
    public void setScreenParent(ScreensController screenParent) {
         myController = screenParent;
    }
    
    @FXML
    public void aoClicarBtnEntrar() throws ServiceException{
        
        Usuario usuario = new Usuario();
        
        if(!Tlogin.getText().isEmpty() && !Tsenha.getText().isEmpty()){
            
            usuario.setNlogin(Tlogin.getText());
            usuario.setSenha(Integer.parseInt(Tsenha.getText()));

            Boolean validacao = loginService.validar(usuario);

            if(validacao == false){
                lbmsg.setText("Usuário não encontrado!");
            }else{
    //            lbmsg.setText("Login feito com sucesso!");
                 myController.setScreen(Trabalho.screen8ID);
            }
        }else{
        
            lbmsg.setText("Campos Obrigatórios!");
        }
        
    }
}
