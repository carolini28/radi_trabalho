/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package trabalho;

import java.awt.event.MouseEvent;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Callback;

/**
 * FXML Controller class
 *
 * @author ra143759
 */
public class FXMLProdutoController implements Initializable,ControlledScreen{

    ScreensController myController;
    ObservableList<Produto> dados; 
    //Gerenciador de dados // CRUD
    ProdutoService prodService = new ProdutoService();
    
    @FXML
    private Label lbmsg;
    
    @FXML
    private TableView<Produto> tvProdutos;
    
    @FXML
    private TextField idP;
    
    @FXML
    private TextField descricaoP;
     
    @FXML
    private TextField valorP;
    
    @FXML
    private TableColumn pid;
    
    @FXML
    private TableColumn pdescricao;
    
    @FXML
    private TableColumn pvalor;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
    
    public void setScreenParent(ScreensController screenParent){
        myController = screenParent;
    }
    
     @FXML
     public void goToScreenP(ActionEvent event){
        myController.setScreen(Trabalho.screen1ID);
     }
    
     @FXML
     private void aoClicarBtnSalvar(ActionEvent event) {
  
        Produto produto = new Produto();
       
        if(!idP.getText().isEmpty()){
            produto.setIdproduto(Integer.parseInt(idP.getText()));
        }else{
             produto.setIdproduto(null);
        }
        produto.setDescricao(descricaoP.getText());
        produto.setValor(valorP.getText());
        
        try {
            prodService.salvar(produto);
            //Mensagem
            lbmsg.setText("Produto Salvo com Sucesso!");
        } catch (ServiceException ex) {
            lbmsg.setText(ex.getMessage());
            Logger.getLogger(FXMLProdutoController.class.getName()).log(Level.SEVERE, null, ex);
        }

     } 
    
     @FXML
     private void aoClicarBtnExcluir(ActionEvent event) {
        Integer id = Integer.parseInt(idP.getText());
        prodService.excluir(id);
        lbmsg.setText("Produto Excluído com Sucesso!");
     }
     
     @FXML
     public void aoClicarBtnBuscarTodos(){

              tvProdutos.setEditable(true);
        
              pid.setCellValueFactory(
                      new PropertyValueFactory<Produto, String>("idproduto") );
              
              pdescricao.setCellValueFactory(
                      new PropertyValueFactory<Produto, String>("descricao"));
              
              pvalor.setCellValueFactory(
                      new PropertyValueFactory<Produto, Double>("valor"));
              
//            TableColumn id = new TableColumn("id");
//            id.setMinWidth(100);
//            id.setCellValueFactory(
//                    new PropertyValueFactory<Produto, String>("idproduto"));
//
//            TableColumn descricao = new TableColumn("descricao");
//            descricao.setMinWidth(100);
//            descricao.setCellValueFactory(
//                    new PropertyValueFactory<Produto, String>("descricao"));
//
//            TableColumn valor = new TableColumn("valor");
//            valor.setMinWidth(100);
//            valor.setCellValueFactory(
//                    new PropertyValueFactory<Produto, String>("valor"));
//
//            tvProdutos.getColumns().addAll(id, descricao, valor);
//            
            //Lista de Produtos
            List<Produto> listaProduto =  prodService.buscarTodos();
            //Inserindo a lista de um Observable
            dados = FXCollections.observableArrayList(listaProduto);

            tvProdutos.setItems(dados);
            tvProdutos.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        
            tvProdutos.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observableValue, Object oldValue, Object newValue) {
                //Check whether item is selected and set value of selected item to Label
                if(tvProdutos.getSelectionModel().getSelectedItem() != null) 
                {    
                   TableViewSelectionModel selectionModel = tvProdutos.getSelectionModel();
                   ObservableList selectedCells = selectionModel.getSelectedCells();
                   TablePosition tablePosition = (TablePosition) selectedCells.get(0);
                   //Object val = tablePosition.getTableColumn().getCellData(newValue);
                   int row = tablePosition.getRow();
                   tvProdutos.getSelectionModel().select(row);
                   Produto prod = tvProdutos.getSelectionModel().getSelectedItem();
                   
                   idP.setText(String.valueOf(prod.getIdproduto()));
                   descricaoP.setText(prod.getDescricao());
                   valorP.setText(prod.getValor());
//                   System.out.println("ID: "+prod.getIdproduto());
//                   System.out.println("Descricao: "+prod.getDescricao());
//                   System.out.println("valor" + prod.getValor());
                 }
                 }
             });
            
    }


    
}
