/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package trabalho;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author ra143759
 */
public class Trabalho extends Application {
    
    public static String screen1ID = "FXMLPrincipal";
    public static String screen1File = "FXMLPrincipal.fxml";
    public static String screen2ID = "FXMLDocumentCliente";
    public static String screen2File = "FXMLDocumentCliente.fxml";
    public static String screen3ID = "FXMLPedido";
    public static String screen3File = "FXMLPedido.fxml";
    public static String screen4ID = "FXMLAgenda";
    public static String screen4File = "FXMLAgenda.fxml";
    public static String screen5ID = "FXMLUsuario";
    public static String screen5File = "FXMLUsuario.fxml";
    public static String screen6ID = "FXMLProduto";
    public static String screen6File = "FXMLProduto.fxml";
    public static String screen7ID = "FXMLTipoEvento";
    public static String screen7File = "FXMLTipoEvento.fxml";
    public static String screen8ID = "FXMLMenu";
    public static String screen8File = "FXMLMenu.fxml";
    public static String screen9ID = "FXMLlogin";
    public static String screen9File = "FXMLlogin.fxml";
    

    
    
    
    @Override
    public void start(Stage stage) throws Exception {
        
        ScreensController mainContainer = new ScreensController();
        mainContainer.loadScreen(Trabalho.screen1ID, Trabalho.screen1File);
        mainContainer.loadScreen(Trabalho.screen2ID, Trabalho.screen2File);
        mainContainer.loadScreen(Trabalho.screen3ID, Trabalho.screen3File);
        mainContainer.loadScreen(Trabalho.screen4ID, Trabalho.screen4File);
        mainContainer.loadScreen(Trabalho.screen5ID, Trabalho.screen5File);
        mainContainer.loadScreen(Trabalho.screen6ID, Trabalho.screen6File);
        mainContainer.loadScreen(Trabalho.screen7ID, Trabalho.screen7File);
        mainContainer.loadScreen(Trabalho.screen8ID, Trabalho.screen8File);
        mainContainer.loadScreen(Trabalho.screen9ID, Trabalho.screen9File);
       
        mainContainer.setScreen(Trabalho.screen9ID);
        
        
        
        
        Group root = new Group();
        root.getChildren().addAll(mainContainer);
        Scene scene = new Scene(root);
        scene.getStylesheets().add(this.getClass().getResource("login.css").toExternalForm());
        stage.setScene(scene);
        stage.show();
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
