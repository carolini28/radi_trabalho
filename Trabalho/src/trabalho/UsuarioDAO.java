/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Carolini
 */
public class UsuarioDAO {
    
    Connection conexao;
    
    public UsuarioDAO(){
        conexao = ConexaoUtil.getConnection();
    }
    
    public void salvar(Usuario usuario) {
        if (usuario.getIdusuario() == null) {
            cadastrar(usuario);
        } else {
            alterar(usuario);
        }
    }

    private void cadastrar(Usuario usuario) {
         String sql = "insert  into usuario (nome, email, login, senha) values (?,?,?,?)";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            preparadorSQL.setString(1, usuario.getNome());
            preparadorSQL.setString(2, usuario.getEmail());
            preparadorSQL.setString(3, usuario.getNlogin());
            preparadorSQL.setInt(4, usuario.getSenha());
            preparadorSQL.execute();
            preparadorSQL.close();
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void alterar(Usuario usuario) {
        String sql = "update usuario set nome=? ,email=?, login=?, senha=? where idusuario=?";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            preparadorSQL.setString(1, usuario.getNome());
            preparadorSQL.setString(2, usuario.getEmail());
            preparadorSQL.setString(3, usuario.getNlogin());
            preparadorSQL.setInt(4, usuario.getSenha());
            preparadorSQL.setInt(5, usuario.getIdusuario());
            
            preparadorSQL.execute();
            preparadorSQL.close();
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public void excluir(Integer id) {
        String sql = "delete from usuario where idusuario=?";

        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            preparadorSQL.setInt(1, id);

            preparadorSQL.execute();
            preparadorSQL.close();
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<Usuario> buscarTodos() {
        String sql = "select * from usuario order by idusuario";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            //Armazenando Resultado da consulta
            ResultSet resultado = preparadorSQL.executeQuery();
            List<Usuario> lista = new ArrayList<Usuario>();
            while (resultado.next()) {
                //Instancia de usuario
                Usuario usuario = new Usuario();

                //Atribuindo dados do resultado no objeto usuario
                usuario.setIdusuario(resultado.getInt("idusuario"));
                usuario.setNome(resultado.getString("nome"));
                usuario.setEmail(resultado.getString("email"));
                usuario.setNlogin(resultado.getString("login"));
                usuario.setSenha(resultado.getInt("senha"));
                
                
                //Adicionando usuario na lista
                lista.add(usuario);
            }
            
            preparadorSQL.close();
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
