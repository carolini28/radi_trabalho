/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author Carolini
 */
public class FXMLUsuarioController implements Initializable,  ControlledScreen {
    
    ScreensController myController;
    
    @FXML
    private TextField tfid;
    
    @FXML
    private TextField tfnome;

    @FXML
    private TextField tfemail;
    
    @FXML
    private TextField tflogin;
    
    @FXML
    private TextField tfsenha;
      
    @FXML
    private Label lbmsg;
    
    @FXML
    private TableView<Usuario> tvUsuario;
    
    @FXML
    private TableColumn Idusuario;
    
    @FXML
    private TableColumn nomeUsuario;
    
    @FXML
    private TableColumn emailUsuario;
    
    @FXML
    private TableColumn loginUsuario;
    
    @FXML
    private TableColumn senhaUsuario;
    
    //Gerenciador de dados // CRUD
    UsuarioService usuService = new UsuarioService();
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    public void setScreenParent(ScreensController screenParent){
        myController = screenParent;
    }
    
    @FXML
    public void goToScreenP(ActionEvent event){
        myController.setScreen(Trabalho.screen1ID);
    }
    
    @FXML
     private void aoClicarBtnSalvar(ActionEvent event) {
  
        Usuario usuario = new Usuario();
       
        if(!tfid.getText().isEmpty()){
           usuario.setIdusuario(Integer.parseInt(tfid.getText()));
        }else{
            usuario.setIdusuario(null);
        }
        
        usuario.setNome(tfnome.getText());
        usuario.setEmail(tfemail.getText());
        usuario.setNlogin(tflogin.getText());
        usuario.setSenha(Integer.parseInt(tfsenha.getText()));
        
        try {
            usuService.salvar(usuario);
            //Mensagem
            lbmsg.setText("Usuário Salvo com Sucesso!");
        } catch (ServiceException ex) {
            lbmsg.setText(ex.getMessage());
            Logger.getLogger(FXMLUsuarioController.class.getName()).log(Level.SEVERE, null, ex);
        }

     } 
    
     @FXML
     private void aoClicarBtnExcluir(ActionEvent event) {
        Integer id = Integer.parseInt(tfid.getText());
        usuService.excluir(id);
        lbmsg.setText("Usuário Excluído com Sucesso!");
     }
     
     @FXML
     public void aoClicarBtnBuscarTodos(){

            tvUsuario.setEditable(true);

            Idusuario.setCellValueFactory(
                    new PropertyValueFactory<Usuario, Integer>("idusuario"));
    
            nomeUsuario.setCellValueFactory(
                    new PropertyValueFactory<Usuario, String>("nome"));
    
            emailUsuario.setCellValueFactory(
                    new PropertyValueFactory<Usuario, String>("email"));
    
            loginUsuario.setCellValueFactory(
                     new PropertyValueFactory<Usuario, String>("Nlogin"));
    
            senhaUsuario.setCellValueFactory(
                    new PropertyValueFactory<Usuario, String>("senha"));
            
//            TableColumn id = new TableColumn("id");
//            id.setMinWidth(100);
//            id.setCellValueFactory(
//                    new PropertyValueFactory<Usuario, String>("idusuario"));
//
//
//            TableColumn nome = new TableColumn("nome");
//            nome.setMinWidth(100);
//            nome.setCellValueFactory(
//                    new PropertyValueFactory<Usuario, String>("nome"));
//
//            TableColumn email = new TableColumn("email");
//            email.setMinWidth(100);
//            email.setCellValueFactory(
//                    new PropertyValueFactory<Usuario, String>("email"));
//            
//            TableColumn login = new TableColumn("login");
//            login.setMinWidth(100);
//            login.setCellValueFactory(
//                    new PropertyValueFactory<Usuario, String>("Nlogin"));
//
//            TableColumn senha = new TableColumn("senha");
//            senha.setMinWidth(100);
//            senha.setCellValueFactory(
//                    new PropertyValueFactory<Usuario, String>("senha"));
//
//            tvUsuario.getColumns().addAll(id, nome, email, login, senha);

            //Lista de Usuarios
            List<Usuario> listaProduto =  usuService.buscarTodos();
            //Inserindo a lista de um Observable
            final ObservableList<Usuario> dados = FXCollections.observableArrayList(listaProduto);

            tvUsuario.setItems(dados);
            
            tvUsuario.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
                @Override
                public void changed(ObservableValue observableValue, Object oldValue, Object newValue) {
                    //Check whether item is selected and set value of selected item to Label
                    if(tvUsuario.getSelectionModel().getSelectedItem() != null) 
                    {    
                       TableView.TableViewSelectionModel selectionModel = tvUsuario.getSelectionModel();
                       ObservableList selectedCells = selectionModel.getSelectedCells();
                       TablePosition tablePosition = (TablePosition) selectedCells.get(0);
                       //Object val = tablePosition.getTableColumn().getCellData(newValue);
                       int row = tablePosition.getRow();
                       tvUsuario.getSelectionModel().select(row);
                       Usuario usuario = tvUsuario.getSelectionModel().getSelectedItem();
                       tfid.setText(String.valueOf(usuario.getIdusuario()));
                       tfnome.setText(usuario.getNome());
                       tfemail.setText(usuario.getEmail());
                       tflogin.setText(usuario.getNlogin());
                       tfsenha.setText(String.valueOf(usuario.getSenha()));
                    }
                  }
           });
        }
}
