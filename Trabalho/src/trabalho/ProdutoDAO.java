/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package trabalho;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ra143759
 */
public class ProdutoDAO {
    
    Connection conexao;
    
    public ProdutoDAO(){
        conexao = ConexaoUtil.getConnection();
    }
    
    public void salvar(Produto produto) {
        if (produto.getIdproduto() == null) {
            cadastrar(produto);
        } else {
            alterar(produto);
        }
    }

    private void cadastrar(Produto produto) {
        String sql = "insert  into produto (descricao,valor) values (?,?)";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            preparadorSQL.setString(1, produto.getDescricao());
            preparadorSQL.setString(2, produto.getValor());
            preparadorSQL.execute();
            preparadorSQL.close();
        } catch (SQLException ex) {
            Logger.getLogger(ProdutoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void alterar(Produto produto) {
        String sql = "update produto set descricao=? ,valor=? where idproduto=?";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            preparadorSQL.setString(1, produto.getDescricao());
            preparadorSQL.setString(2, produto.getValor());
            preparadorSQL.setInt(3, produto.getIdproduto());
            preparadorSQL.execute();
            preparadorSQL.close();
        } catch (SQLException ex) {
            Logger.getLogger(ProdutoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void excluir(Integer id) {
        String sql = "delete from produto where idproduto=?";

        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            preparadorSQL.setInt(1, id);

            preparadorSQL.execute();
            preparadorSQL.close();
        } catch (SQLException ex) {
            Logger.getLogger(ProdutoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public List<Produto> buscarTodos() {
        String sql = "select * from produto order by idproduto";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            //Armazenando Resultado da consulta
            ResultSet resultado = preparadorSQL.executeQuery();
            List<Produto> lista = new ArrayList<Produto>();
            while (resultado.next()) {
                //Instancia de produtos
                Produto prod = new Produto();

                //Atribuindo dados do resultado no objeto produto
                prod.setIdproduto(resultado.getInt("idproduto"));
                prod.setDescricao(resultado.getString("descricao"));
                prod.setValor(resultado.getString("valor"));
                
                //Adicionando produto na lista
                lista.add(prod);
            }
            
            preparadorSQL.close();
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(ProdutoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    ArrayList<String> BuscarProdutos() {
        String sql = "select * from produto order by idproduto";
        try {
            PreparedStatement preparadorSQL = conexao.prepareStatement(sql);
            //Armazenando Resultado da consulta
            ResultSet resultado = preparadorSQL.executeQuery();
            ArrayList<String> lista = new ArrayList<String>();
            while (resultado.next()) {
     
                lista.add(resultado.getString("descricao"));
                
            }         
            preparadorSQL.close();
            return lista;
        } catch (SQLException ex) {
            Logger.getLogger(ProdutoDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    
}
