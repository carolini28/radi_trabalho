/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho;

/**
 *
 * @author Carolini
 */
public class LoginService {
    
   private LoginDAO loginDAO;
   
   public LoginService(){
       loginDAO = new LoginDAO();
   }
  
   public Boolean validar(Usuario usuario) throws ServiceException{
   
       if (usuario.getNlogin().isEmpty()) {
            throw new ServiceException("Campo Login é obrigatório!");
       }

       if (usuario.getSenha() == null) {
            throw new ServiceException("Campo senha é obrigatório!");
       }
       
       return loginDAO.validar(usuario);
       
   }
    
}
