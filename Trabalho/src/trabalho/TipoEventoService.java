/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package trabalho;

import java.util.List;

/**
 *
 * @author ra143759
 */
public class TipoEventoService {
    
    private TipoEventoDAO eventoDAO;

    public TipoEventoService() {
        eventoDAO = new TipoEventoDAO();
    }
    
    //Decide se vai guardar nova info ou alterar no TipoEventoDAO
    public void salvar(TipoEvento evento) throws ServiceException {
       
        if (evento.getDescricao().isEmpty()) {
            throw new ServiceException("Campo Descrição é Obrigatório!");
        }

        eventoDAO.salvar(evento);
    }
    
    public void excluir(Integer id) {
        eventoDAO.excluir(id);
    }
    
    public List<TipoEvento> buscarTodos() {
            return eventoDAO.buscarTodos();
    }
    
}
