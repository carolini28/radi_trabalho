/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Carolini
 */
public class AgendaService {
 
    private AgendaDAO agendaDAO;
    
    public AgendaService(){
        agendaDAO = new AgendaDAO();
    }
    
    public List<Agenda> buscarAgenda(String cliente, Date dataIE, Date dataFE, Date dataIC, Date dataFC, int busca) throws ParseException{       
            return agendaDAO.buscar(cliente, dataIE, dataFE, dataIC, dataFC, busca);
    }
}
