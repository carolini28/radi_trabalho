/*
PGSQL Backup
Source Server Version: 9.3.2
Source Database: RADI
Date: 13/05/2015 20:01:56
*/


-- ----------------------------
--  Table structure for "public"."cliente"
-- ----------------------------
DROP TABLE "public"."cliente";
CREATE TABLE "public"."cliente" (
"idcliente" int4 DEFAULT nextval('cliente_idcliente_seq'::regclass) NOT NULL,
"nome" varchar COLLATE "default" NOT NULL,
"fone" varchar COLLATE "default" NOT NULL,
"email" varchar COLLATE "default" NOT NULL,
PRIMARY KEY ("idcliente")
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."itempedido"
-- ----------------------------
DROP TABLE "public"."itempedido";
CREATE TABLE "public"."itempedido" (
"iditempedido" int4 DEFAULT nextval('itempedido_iditempedido_seq'::regclass) NOT NULL,
"quantidade" int4 NOT NULL,
"valor" float8 NOT NULL,
"idpedido" int4 NOT NULL,
"idproduto" int4 NOT NULL,
PRIMARY KEY ("iditempedido"),
FOREIGN KEY ("idproduto") REFERENCES "public"."produto" ("idproduto") ON DELETE NO ACTION ON UPDATE NO ACTION,
FOREIGN KEY ("idpedido") REFERENCES "public"."pedido" ("idpedido") ON DELETE NO ACTION ON UPDATE NO ACTION
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."pedido"
-- ----------------------------
DROP TABLE "public"."pedido";
CREATE TABLE "public"."pedido" (
"idpedido" int4 DEFAULT nextval('pedido_idpedido_seq_1'::regclass) NOT NULL,
"origempedido" varchar COLLATE "default" NOT NULL,
"datapedido" date NOT NULL,
"cerimonial" varchar COLLATE "default" NOT NULL,
"dataevento" date NOT NULL,
"horaevento" varchar COLLATE "default" NOT NULL,
"indicacao" varchar COLLATE "default" NOT NULL,
"endereco" varchar COLLATE "default" NOT NULL,
"observacao" varchar COLLATE "default" NOT NULL,
"localevento" varchar COLLATE "default" NOT NULL,
"idcliente" int4 NOT NULL,
"idtipoevento" int4 NOT NULL,
PRIMARY KEY ("idpedido"),
FOREIGN KEY ("idcliente") REFERENCES "public"."cliente" ("idcliente") ON DELETE NO ACTION ON UPDATE NO ACTION,
FOREIGN KEY ("idtipoevento") REFERENCES "public"."tipoevento" ("idtipoevento") ON DELETE NO ACTION ON UPDATE NO ACTION
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."produto"
-- ----------------------------
DROP TABLE "public"."produto";
CREATE TABLE "public"."produto" (
"idproduto" int4 DEFAULT nextval('produto_idproduto_seq'::regclass) NOT NULL,
"descricao" varchar COLLATE "default" NOT NULL,
"valor" varchar COLLATE "default" NOT NULL,
PRIMARY KEY ("idproduto")
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Table structure for "public"."tipoevento"
-- ----------------------------
DROP TABLE "public"."tipoevento";
CREATE TABLE "public"."tipoevento" (
"idtipoevento" int4 DEFAULT nextval('tipoevento_idtipoevento_seq'::regclass) NOT NULL,
"descricao" varchar COLLATE "default" NOT NULL,
PRIMARY KEY ("idtipoevento")
)
WITH (OIDS=FALSE)
;;

-- ----------------------------
--  Records 
-- ----------------------------
INSERT INTO "public"."cliente" VALUES ('0','carolini','123456','carolini@gmail.com'); INSERT INTO "public"."cliente" VALUES ('1','joao','122222','pedro@gmail'); INSERT INTO "public"."cliente" VALUES ('2','lucas','1111111','lucas@gmail.com');
INSERT INTO "public"."itempedido" VALUES ('1','2','6','1','2'); INSERT INTO "public"."itempedido" VALUES ('2','3','6','1','0');
INSERT INTO "public"."pedido" VALUES ('1','origem1','0008-07-07','cerimonial1','0008-07-07','12:00','cliente1','rua1','obs1','local1','2','0');
INSERT INTO "public"."produto" VALUES ('0','brigadeiro','2'); INSERT INTO "public"."produto" VALUES ('1','cupcake','5'); INSERT INTO "public"."produto" VALUES ('2','bem casado','3');
INSERT INTO "public"."tipoevento" VALUES ('0','casamento'); INSERT INTO "public"."tipoevento" VALUES ('1','aniversario'); INSERT INTO "public"."tipoevento" VALUES ('2','reuniao');
